export type IChargingState = "charging" | "occupied" | "available" | "error" | "outOfOrder" | "maintenance";
export interface ICharge {
    id: number,
    startTime: string,
    endTime: string,
    SoC: number,
    carModel: string,
    chargePower: number,
    startCharge: number,
    endCharge: number
}
export interface IPistol {
    name: string,
    type: string,
    maxOutput: string,
    currentOutput: string,
    desc: string,
    charges?: ICharge[],
    hasError: boolean,
    firmware: string,
    serviceHistory?: string,
    connector: string,
    serialNum: string,
    state: IChargingState,
    lastMaintenanceDate?: string,
    nextMaintenanceDate?: string,
    idleTime?: number
}
export class PowerSource {
    type: string;
    power: string;
    firmware: string;
    serialNumber: string;
    pistols: IPistol[]
    constructor() {
        this.type = "Kempower C-Series";
        this.power = "2 X 100kW";
        this.firmware = "1.1.0";
        this.serialNumber = "C1234567",
        this.pistols = [{
            name: 'Charging station 1',
            type: "Kempower S-series 1",
            maxOutput: "80kW",
            currentOutput: "50kW",
            desc: "description",
            state: "available",
            idleTime: 500,
            charges: [
                {
                    id: 1,
                    startTime: "15:30",
                    endTime: "16:00",
                    SoC: 55,
                    carModel: "Pösö",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 2,
                    startTime: "08:30",
                    endTime: "09:30",
                    SoC: 55,
                    carModel: "BMW",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 3,
                    startTime: "11:00",
                    endTime: "12:00",
                    SoC: 55,
                    carModel: "Tesla",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 4,
                    startTime: "12:15",
                    endTime: "13:00",
                    SoC: 55,
                    carModel: "Tesla",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 5,
                    startTime: "14:30",
                    endTime: "15:00",
                    SoC: 55,
                    carModel: "BMW",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 6,
                    startTime: "16:30",
                    endTime: "17:00",
                    SoC: 55,
                    carModel: "BMW",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 7,
                    startTime: "18:30",
                    endTime: "19:00",
                    SoC: 55,
                    carModel: "Tesla",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 8,
                    startTime: "21:30",
                    endTime: "22:00",
                    SoC: 55,
                    carModel: "Toyota",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 9,
                    startTime: "22:15",
                    endTime: "23:00",
                    SoC: 55,
                    carModel: "Opel",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                },
                {
                    id: 10,
                    startTime: "00:30",
                    endTime: "01:00",
                    SoC: 55,
                    carModel: "Fiat",
                    startCharge: 20,
                    endCharge: 50,
                    chargePower: 50
                }
            ],
            hasError: true,
            firmware: "1.2.0",
            connector: "CCS",
            nextMaintenanceDate: "20.02.2020",
            serialNum: "S001001 / S001010 / S001011"
        },
        {
            name: 'Charging station 2',
            type: "Kempower S-series 2",
            maxOutput: "80kW",
            currentOutput: "40kW",
            desc: "description",
            state: "charging",
            charges: [{
                id: 1,
                startTime: "15:30",
                endTime: "16:00",
                SoC: 55,
                carModel: "Pösö",
                startCharge: 20,
                endCharge: 50,
                chargePower: 50,
            }],
            idleTime: 500,
            hasError: false,
            firmware: "1.2.0",
            connector: "CCS",
            nextMaintenanceDate: '05.07.2020',
            serialNum: "S001001 / S001010 / S001011"
        },
        {
            name: 'Charging station 3',
            type: "Kempower S-series 3",
            maxOutput: "80kW",
            currentOutput: "70kW",
            desc: "description",
            state: "available",
            charges: [{
                id: 1,
                startTime: "15:30",
                endTime: "16:00",
                SoC: 55,
                carModel: "Pösö",
                startCharge: 20,
                endCharge: 50,
                chargePower: 50
            },{
                id: 2,
                startTime: "15:30",
                endTime: "16:00",
                SoC: 55,
                carModel: "Pösö",
                startCharge: 20,
                endCharge: 50,
                chargePower: 50
            }],
            idleTime: 400,
            hasError: false,
            firmware: "1.2.0",
            serviceHistory: "this has been served before",
            connector: "CCS",
            nextMaintenanceDate: "27.06.2020",
            serialNum: "S001001 / S001010 / S001011"
        }]
    }
}
