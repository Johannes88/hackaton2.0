import { Component, OnInit } from '@angular/core';
import { PistolsService } from 'src/app/services/pistols.service';
import { IPistol } from 'src/app/data-models/charge-model';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  actions: string[];
  action: string;

  pistols: IPistol[] = [];
  constructor(private pistolService: PistolsService) {
    this.pistols = pistolService.getPistols();

  }

  ngOnInit() {

    this.actions=["restart", "shutdown", "switch on", "service mode"];

  }
  onChecked(action){
    this.action = action;
    console.log(action);
  }
  onApply(){
    alert("Station service status: " + this.action);
  }


}
