import { Component, OnInit } from '@angular/core';
import { IPistol, PowerSource } from "./../data-models/charge-model";
import { PistolsService } from '../services/pistols.service';
@Component({
  selector: 'app-controll-panel',
  templateUrl: './controll-panel.component.html',
  styleUrls: ['./controll-panel.component.css']
})
export class ControllPanelComponent implements OnInit {
  powersource: PowerSource; 
  pistols: IPistol[];
  constructor(private pistolService: PistolsService) {
    this.powersource = new PowerSource();
    this.pistols = this.pistolService.getPistols();
  }

  ngOnInit() {
  }

}
