import { Component, OnInit } from '@angular/core';
import { IPistol } from "./../../data-models/charge-model";
import { PistolsService } from 'src/app/services/pistols.service';
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  pistols: IPistol[] = [];
  constructor(private pistolService: PistolsService) {
    this.pistols = pistolService.getPistols();
  }

  ngOnInit() {
  }
  changeName(pistol) {
    let name = prompt("anna nimi:");
    pistol.name = name;
  }
  changeMaxOutPut(pistol) {
    let maxOutput = prompt("anna maxOutPut:");
    pistol.maxOutput = maxOutput;
  }
  changeDesc(pistol) {
    let desc = prompt("anna desc:");
    pistol.desc = desc;
  }
}
