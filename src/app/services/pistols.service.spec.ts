import { TestBed } from '@angular/core/testing';

import { PistolsService } from './pistols.service';

describe('PistolsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PistolsService = TestBed.get(PistolsService);
    expect(service).toBeTruthy();
  });
});
