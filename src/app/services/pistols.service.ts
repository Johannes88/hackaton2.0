import { Injectable } from '@angular/core';
import { IPistol, PowerSource } from "./../data-models/charge-model";
@Injectable({
  providedIn: 'root'
})
export class PistolsService {
  powersource: PowerSource = new PowerSource();

  constructor() {
  }
  getPowerSourceInfo() {
    return
  }
  getPistols(): IPistol[] {
    return this.powersource.pistols;
  }
  getOnePistol(id: number): IPistol {
    return this.powersource.pistols[id];
  }

}
