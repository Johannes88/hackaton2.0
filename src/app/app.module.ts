import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { ControllPanelComponent } from './controll-panel/controll-panel.component';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReportComponent } from './controll-panel/report/report.component';
import { ServiceComponent } from './controll-panel/service/service.component';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatRadioModule} from '@angular/material/radio';

import { RouterModule, Routes } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
const appRoutes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'controlpanel', component: ControllPanelComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    ControllPanelComponent,
    ReportComponent,
    ServiceComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatRadioModule,
    MatTabsModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatToolbarModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
