import { Component, OnInit } from '@angular/core';
import { IPistol } from 'src/app/data-models/charge-model';
import { PistolsService } from 'src/app/services/pistols.service';

export enum stateColor {
  available = "green",
  charging = "blue",
  occupied = "yellow",
  failed = "red"
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  allChargers: IPistol[];
  stateColor: any;
  soc: number[];
  constructor(private pistolService: PistolsService) {
    this.allChargers = this.pistolService.getPistols();
    this.soc = this.allChargers.map(e => {
      return e.charges.map(e => e.chargePower).reduce((total, num) => {
        return total + num
      })
    })
    this.stateColor = {
      available: "green",
      charging: "blue",
      occupied: "yellow",
      failed: "red"
   }
   }
}
